import java.util.Arrays;

public class Matrix {
    double[][] t;
	int cursorx;
	int cursory;
	int rang;
	int[] libresInd;
	double[][] equation;
	/**
	 * @return String of Matrix information's
	 */
	@Override
	public String toString(){
		return resolver.printArr(t) + "Rang : " + rang;
	}
	/**
	 * Test if all childrens have same length
	 * @param t any Bidimentionnal array
	 * @return boolean if all elements of t have same length
	 */
	private boolean isMatrix(double[][] t){
		int l;

		if(t.length == 0) return false;
		else l = t[0].length;
		for(int i = 1; i < t.length; i++){
			if(t[i].length != l) return false;
		}
		return true;
	}
	private boolean etape1(){
		int j = cursory;
		if(t[j][cursorx] != 0) return true;
		while(j < t.length){
			if(t[j][cursorx] != 0){
				double[] temp = t[j];
				t[j] = t[cursory];
				t[cursory] = temp;
				return true;
			}
			j++;
		}
		cursorx++;
		return cursorx < t[0].length ? etape1() : false;
	}
	private void etape2(){
		if(t[cursory][cursorx] != 1 && t[cursory][cursorx] != 0){
			double diviser = t[cursory][cursorx];
			for(int i = 0; i < t[cursory].length; i++){
				t[cursory][i] = t[cursory][i]/diviser;
			}
		}
	}
	private void etape3(){
		for(int y = 0; y < t.length; y++){
			if(t[y][cursorx] != 0 && y != cursory){
				double multiplier = t[y][cursorx];
				for(int x = 0; x < t[cursory].length; x++){
					t[y][x] -= t[cursory][x]*multiplier;
				}
			}
		}
	}
	private boolean etape4(){
		cursorx++;
		cursory++;
		return cursorx < t[0].length-1 && cursory < t.length;
	}
	private int searchLibresInd(int index, boolean write){
		int k = 0;
		for(int i = 0; i < libresInd.length; i++){
			if(libresInd[i] == index) return i;
			if(libresInd[i] != -1) k++;
		}
		if(write){
			libresInd[k] = index;
			return k+1;
		}
		else return -1;
	}
	/**
	 * Return String representation of equation of solutions
	 * @return
	 */
	public String toStringEq(){
		String s = "Xn\tDec\t";
		char alpha = 'a';
		//1ere ligne
		for(int i = 1; i < equation[0].length; i++){
			int decLetter = alpha + i-1;
			char letter = (char)decLetter;
			s += letter + "\t";
		}
		s += '\n';
		for(int i = 0; i < equation.length; i++){
			s += i + "\t";
			for(int j = 0; j < equation[i].length; j++){
				s += equation[i][j] + "\t";
			}
			s += "\n";
		}
		return s;
	}
	/**
	 * Generate equation by solutions
	 */
	public void generateEq(){
		int nbLibres = (t[0].length-1) - rang;
		double[][] res = new double[t.length][1 + nbLibres];
		
		libresInd = new int[nbLibres];
		for(int i = 0; i < libresInd.length; i++) libresInd[i] = -1;

		for(int y = 0; y < t.length; y++)
		{
			int xn = searchLibresInd(y, false);
			if(xn == -1){
				for(int x = 0; x < t[0].length-1; x++){
					if(t[y][x] != 0){
						if(t[y][x] == 1){
							res[y][0] = t[y][t[0].length-1];
						}
						else{
							int ind = searchLibresInd(x, true);
							res[y][ind] = -t[y][x];
						}
					}
				}
			}
			else{
				res[y][xn+1] = 1;
			}
		}		
		equation = res;
	}
	public String saySolutions(){
		if(rang < t.length){
			if(rang == t[0].length-1) return "0 solution";
			else return "infinity of solutions";
		}
		else return "1 solution";

	}
	public void resolve(){
		boolean run = true;
		while(run){
			if(etape1()){
				rang++;	
				etape2();
				etape3();	
				run = etape4();
			}
			else run = false;
		}
		System.out.println(toString());
		System.out.println(saySolutions());
		generateEq();
		System.out.println(toStringEq());

	}
	/**
	 * Generate new Matrix with t[][]
	 * @param t Bidimentionnal rectangular array
	 */
	public Matrix(double[][] t){
		this.cursorx = 0;
		this.cursory = 0;
		this.rang = 0;
		if(isMatrix(t)){
			this.t = new double[t.length][t[0].length];
			for(int i = 0; i < t.length; i++){
				this.t[i] = Arrays.copyOf(t[i], t[i].length);	
			}
		}
	}
}
