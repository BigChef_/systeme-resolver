import java.util.Scanner;

public class resolver {
	/**
	 * @param t is rectangular
	 * @return String representation of array
	 */
	public static String printArr(double[][] t){
		String s = "";
		//1ere ligne
		for(int i = 0; i < t[0].length-1; i++){
			s += "  x" + i + "\t";	
		}
		s += "Solution\n";
		for(int i = 0; i < t.length; i++){
			s += "";
			for(int j = 0; j < t[i].length; j++){
				s += t[i][j] + "\t";
			}
			s += "\n";
		}
		return s;
	}
	/**
	 * Ask user to fill array from System.in
	 * @return bidimentionnal rectangular array
	 */
	private static double[][] writeArr(){
		
		int cursorx = 0;
		int cursory = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Longueur m : ");
		int x = sc.nextInt()+1;
		
		System.out.print("Hauteur n : ");
		int y = sc.nextInt();
		double[][] r = new double[y][x];
		
		String res;

		System.out.println(printArr(r));
		while(cursory != y){
			if(cursorx == x){
				cursorx = 0;
				cursory++;
			}
			else {
				res = sc.next();
				r[cursory][cursorx] = Double.parseDouble(res);
				cursorx++;
				System.out.println(printArr(r));
			}
		}
		sc.close();
		return r;
	}
	public static void main(String[] args){
		//Si vous souhaitez le configurer vous même, décommentez/recommentez les lignes
		//double[][] p = {{0, 0, 1, -1, -1, 4}, {2, 4, 2, 4, 2, 4}, {2, 4, 3, 3, 3, 4}, {3, 6, 6, 3, 6, 6}};
		//Matrix m =  new Matrix(p);
		Matrix m =  new Matrix(writeArr());
		m.resolve();
	}
	//Pas en static final => eclipse prends pas en charge et on demande si on les veux

}
